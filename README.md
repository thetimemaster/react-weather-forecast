## Summary

A simple React app serving as a frontend for
[OpenWeatherMap One Call API](https://openweathermap.org/api/one-call-api).

The weather forecast can be checked for variety of cities and current geolocation and toggled between short term 
hourly forecast and long term daily one. For each query the app also heuristically estimates if the weather is
nice and shows GIF depicting the weather using [Tenor GIF API](https://tenor.com/gifapi). 

The application was created as a final assignment for my Languages and tools for programming II
course on University of Warsaw, React + Redux group. 

## Used frameworks and libraries

* [React](https://reactjs.org/) for obvious reasons
* [Redux](https://redux.js.org/) with official binding [react-redux](https://react-redux.js.org/) and [Immutable.js](https://immutable-js.github.io/immutable-js/) for state handling
* [RxJS](https://rxjs-dev.firebaseapp.com/) with [Redux Observable](https://redux-observable.js.org/) for async operations handling
* [Reselect](https://github.com/reduxjs/reselect) for state selection and retrieval
* [Styled components](https://styled-components.com/) for themes and general ease of styling
* [Trie-Search](https://www.npmjs.com/package/trie-search) for efficient Trie implementation for city search

## Available Scripts

In the project directory you can run classic `create react app` scripts:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

Test and eject scripts were not removed as they served no purpose here.
