import {Theme} from "./const";
import {defaultQueryState, QueryState} from "./containers/query-displayer/queryState";
import * as Immutable from 'immutable';
import {defaultForecastState, ForecastState} from "./containers/weather-forecast-displayer/forecastState";

export type WeatherAppState = Immutable.Record<{
    theme: Theme,
    queryState: QueryState,
    forecastState: ForecastState,
    ajaxCache: Immutable.Map<string, any>
}>;

export const defaultState: WeatherAppState = Immutable.fromJS({
    theme: Theme.LIGHT,
    queryState: defaultQueryState,
    forecastState: defaultForecastState,
    ajaxCache: Immutable.fromJS({})
});
