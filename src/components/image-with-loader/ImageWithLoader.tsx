import React, {FunctionComponent, useState} from "react";
import FillLoader from "../fill-loader/FillLoader";
import styled from "styled-components";

type ImageWithLoaderProps = {
    src: string
}

const FillingImage = styled.div`
  display: inline-block;
  width: 100%;
  height: 100%;
  background-image: url("${(p: {imgSrc: string}) => p.imgSrc}");
  background-size: contain;
  background-position: center;
  background-repeat: no-repeat;
`

const ImageWithLoader: FunctionComponent<ImageWithLoaderProps> = props => {
    const [loaded, setLoaded] = useState(false);
    const [src, setSrc] = useState(props.src);

    if (src !== props.src) {
        setSrc(props.src);
        setLoaded(false);
    }

    const img = new Image();
    img.onload = () => setLoaded(true);
    img.src = props.src;

    if (loaded) return (
        <FillingImage imgSrc={props.src}/>
    );

    return <FillLoader/>
}

export default ImageWithLoader;
