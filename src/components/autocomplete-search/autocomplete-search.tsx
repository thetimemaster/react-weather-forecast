import React, {FunctionComponent} from "react";
import Autocomplete from "react-autocomplete";
import styled from "styled-components";
import {IStyleArgument} from "../../styles";

export type AutocompleteSearchProps = {
    onSelect: (sel: string) => any;
    items: string[];
    currentValue: string;
}

const Input = styled.input`
  border: 2px solid ${(p: IStyleArgument) => p.theme.secondary};
  background-color: ${(p: IStyleArgument) => p.theme.background};
  color: ${(p: IStyleArgument) => p.theme.text};
  
  height: 30px;
  margin-bottom: 5px;
  margin-top: 5px;
  border-radius: 5px;
  padding: 2px;
  font-family: Sans, serif;
`


const AutocompleteSearch: FunctionComponent<AutocompleteSearchProps> = props => (
    <Autocomplete
        items={props.items}
        value={props.currentValue}
        getItemValue={i => i}
        renderItem={(i, isHighlighted) =>
            <div style={{ background: isHighlighted ? 'lightgray' : 'white' }}>
                {i}
            </div>
        }
        onChange={(e, v) => props.onSelect(v)}
        onSelect={props.onSelect}
        wrapperStyle={{
            float: 'left',
        }}
        // @ts-ignore
        renderInput={props => <Input {...props}/>}
    />
);

export default AutocompleteSearch;
