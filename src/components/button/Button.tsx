import React, {FunctionComponent} from "react";
import styled from 'styled-components';
import {IStyleArgument} from "../../styles";

export enum ButtonFloat {
    NONE = 'initial',
    RIGHT = 'right',
    LEFT = 'left',
}

export type ButtonProps = {
    text: string,
    onClick: () => void,
    isDisabled: boolean,
    float?: ButtonFloat,
}

export interface IWithFloat {
    float: ButtonFloat
}

const StyledButton = styled.button<IWithFloat>`
  background-color: ${(p: IStyleArgument) => p.theme.secondary};
  color: ${(p: IStyleArgument) => p.theme.textOnSecondary};
  float: ${(p: IWithFloat) => p.float};
  
  margin: 5px;
  padding: 5px;
  border: none;
  border-radius: 5px;
  height: 30px;
  min-height: 30px;
  line-height: 20px;
  min-width: 120px;
  flex-basis: 0;
  
  &:not(:disabled):hover {
    background-color:${(p: IStyleArgument) => p.theme.secondaryDark};
  }
  
  &:disabled,
  &[disabled] {
    opacity: 0.6;
  }
`;

const Button: FunctionComponent<ButtonProps> = (props) => (
    <StyledButton onClick={props.onClick} disabled={props.isDisabled} float={props.float ? props.float : ButtonFloat.NONE}>
        {props.text}
    </StyledButton>
);

export default Button;

