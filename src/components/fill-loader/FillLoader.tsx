import React, {FunctionComponent} from "react";
import Loader from "react-loader-spinner";
import { withTheme } from 'styled-components';
import {ITheme} from "../../styles";
import CenterFill from "../center-fill-wrapper/CenterFill";

const FillLoader: FunctionComponent<{theme: ITheme}> = (props) => (
    <CenterFill>
        <Loader color={props.theme.primary}/>
    </CenterFill>
);

export default withTheme(FillLoader);
