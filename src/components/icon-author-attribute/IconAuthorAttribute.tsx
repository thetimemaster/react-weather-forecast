import styled from "styled-components";
import React, {FunctionComponent} from "react";
import {IStyleArgument} from "../../styles";

const IconAuthorAttributeWrapper = styled.div`
  position: fixed;
  right: 0;
  bottom: 0;
  padding: 5px;
  color: ${(p: IStyleArgument) => p.theme.text};
  
  & a {
    text-decoration: none;
    color: ${(p: IStyleArgument) => p.theme.primary};
  }
  
  & a:hover {
    color: ${(p: IStyleArgument) => p.theme.primaryDark};
  }
`

const IconAuthorAttribute: FunctionComponent = () =>
    <IconAuthorAttributeWrapper>
        Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
    </IconAuthorAttributeWrapper>

export default IconAuthorAttribute;

