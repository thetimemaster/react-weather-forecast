import styled from "styled-components";
import {FunctionComponent} from "react";
import React from "react";

const OuterWrapper = styled.div`
  display: inline-block;
  width: 100%;
  height: 100%;
  position:relative;
`;

const InnerWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

const CenterFill: FunctionComponent = props => (
    <OuterWrapper>
        <InnerWrapper>
            {props.children}
        </InnerWrapper>
    </OuterWrapper>
);

export default CenterFill;
