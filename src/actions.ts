import {QueryAction, QueryActionType} from "./containers/query-displayer/queryActions";
import {ForecastAction, ForecastActionType} from "./containers/weather-forecast-displayer/forecastActions";

export enum BaseActionType {
    TOGGLE_THEME = "base/ToggleTheme",
    API_CALL = "base/ApiCall",
    CACHE_API_RESPONSE = "base/CacheApiResponse",
}


export type ToggleThemeAction = {
    type: BaseActionType.TOGGLE_THEME,
}
export const MakeToggleThemeAction = () => ({
    type: BaseActionType.TOGGLE_THEME,
}) as WeatherAppAction;


export type ApiCallAction = {
    type: BaseActionType.API_CALL,
    route: string,
    responseActionType: ActionType,
    responseActionFields: any,
}
export const MakeApiCallAction = (route: string, responseActionType: ActionType, responseActionFields: any) => ({
    type: BaseActionType.API_CALL,
    route,
    responseActionType,
    responseActionFields,
}) as WeatherAppAction;


export type CacheApiResponseAction = {
    type: BaseActionType.CACHE_API_RESPONSE,
    route: string,
    result: any,
}
export const MakeCacheApiResponseAction = (route: string, result: any) => ({
    type: BaseActionType.CACHE_API_RESPONSE,
    route,
    result,
}) as WeatherAppAction;


export type BaseAction =
      ToggleThemeAction
    | ApiCallAction
    | CacheApiResponseAction;

export type ActionType =
      BaseActionType
    | QueryActionType
    | ForecastActionType;

export type WeatherAppAction =
      QueryAction
    | ForecastAction
    | BaseAction;

