import {combineEpics, Epic} from "redux-observable";
import {MakeApiCallAction, WeatherAppAction} from "../actions";
import {WeatherAppState} from "../state";
import {ForecastActionType, MakeSetGifUrlAction, SelectGifUrlAction} from "../containers/weather-forecast-displayer/forecastActions";
import {Observable} from 'rxjs/Rx';
import {selectGifKeyword} from "../containers/weather-forecast-displayer/forecastSelectors";

const KEY = 'ZD4JZQNF2J4D';

const catchForecastDisplayChange: Epic<WeatherAppAction, WeatherAppAction, WeatherAppState> = (action$, state$) =>
    action$.ofType(ForecastActionType.RECEIVE_API_DATA, ForecastActionType.TOGGLE_FORECAST_MODE)
        .switchMap(action =>
            Observable.timer(0,30000)
                .map<number, WeatherAppAction>(
                    (i: number) => MakeApiCallAction(
                        `https://api.tenor.com/v1/search?q=${selectGifKeyword(state$.value)}&key=${KEY}&limit=32`,
                        ForecastActionType.SELECT_SOME_GIF,
                        {}
                    )
                )
        );

const parseGifResponse: Epic<WeatherAppAction, WeatherAppAction, WeatherAppState> = (action$, state$) =>
    action$.ofType(ForecastActionType.SELECT_SOME_GIF)
        .map(action => {
            action = action as SelectGifUrlAction;
            const index = Date.now() % action.data.results.length;

            return MakeSetGifUrlAction(action.data.results[index].media[0].gif.url);
        });

export const gifFetcherEpic: Epic<WeatherAppAction, WeatherAppAction, WeatherAppState> = combineEpics(
    catchForecastDisplayChange,
    parseGifResponse,
);
