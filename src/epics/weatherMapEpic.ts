import {combineEpics, Epic} from "redux-observable";
import {MakeApiCallAction, WeatherAppAction} from "../actions";
import {WeatherAppState} from "../state";
import {QueryActionType} from "../containers/query-displayer/queryActions";
import {selectCoords, selectDescription} from "../containers/query-displayer/querySelectors";
import {ForecastActionType} from "../containers/weather-forecast-displayer/forecastActions";

const API_KEY = "09ef28d45db67c405263e7f34287e69c";

const catchCommit: Epic<WeatherAppAction, WeatherAppAction, WeatherAppState> = (action$, state$) =>
    action$.ofType(QueryActionType.COMMIT)
        .filter(() => selectCoords(state$.value) !== undefined)
        .map(() => {
            const coords = selectCoords(state$.value);
            const route = `https://api.openweathermap.org/data/2.5/onecall?lat=${coords?.lat}&lon=${coords?.lon}&appid=${API_KEY}`;
            return MakeApiCallAction(
                route,
                ForecastActionType.RECEIVE_API_DATA,
                {
                    newLocation: selectDescription(state$.value)
                }
            )
        });

export const weatherMapEpic: Epic<WeatherAppAction, WeatherAppAction, WeatherAppState> = combineEpics(
    catchCommit
);
