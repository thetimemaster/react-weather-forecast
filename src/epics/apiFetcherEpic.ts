import {Epic} from "redux-observable";
import {ApiCallAction, BaseActionType, WeatherAppAction} from "../actions";
import {WeatherAppState} from "../state";
import {Observable} from 'rxjs/Rx';

export const apiFetcherEpic: Epic<WeatherAppAction, WeatherAppAction, WeatherAppState> = (action$, state$) =>
    action$.ofType(BaseActionType.API_CALL)
        .mergeMap(action => {
            const apiCallAction = action as ApiCallAction;
            const ajaxCache = state$.value.get("ajaxCache");

            if (ajaxCache.has(apiCallAction.route)) {
                return Observable.of({
                    ...apiCallAction.responseActionFields,
                    type: apiCallAction.responseActionType,
                    data: ajaxCache.get(apiCallAction.route)
                } as WeatherAppAction);

            } else {
                return Observable.ajax.get(apiCallAction.route)
                    .mergeMap(ajaxObj => [
                        {
                            type: BaseActionType.CACHE_API_RESPONSE,
                            route: apiCallAction.route,
                            result: ajaxObj.response
                        },
                        {
                            ...apiCallAction.responseActionFields,
                            type: apiCallAction.responseActionType,
                            data: ajaxObj.response
                        }
                    ]);
            }
        });
