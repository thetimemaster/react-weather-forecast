import {WeatherAppAction} from "../../actions";

const QUERY_ACTION_PREFIX = 'query';

export const isQueryAction = (action: WeatherAppAction): action is QueryAction => {
    return action.type.startsWith(QUERY_ACTION_PREFIX);
}

export enum QueryActionType {
    SET_CITY = `query/SetCity`,
    SET_GEO_LOC = `query/SetGeoLoc`,
    SET_AVAILABLE_CITIES = `query/SetAvailableCities`,
    COMMIT = `query/Commit`,
}


export type CommitQueryAction = {
    type: QueryActionType.COMMIT,
}
export const MakeCommitQueryAction = () => ({
    type: QueryActionType.COMMIT
}) as WeatherAppAction;


export type SetGeoLocAction = {
    type: QueryActionType.SET_GEO_LOC,
    lat: number,
    lng: number,
}
export const MakeSetGeoLocAction = (lat: number, lng: number) => ({
    type: QueryActionType.SET_GEO_LOC,
    lat,
    lng,
}) as WeatherAppAction;


export type SetCityAction = {
    type: QueryActionType.SET_CITY,
    city: string,
}
export  const MakeSetCityAction = (city: string) => ({
    type: QueryActionType.SET_CITY,
    city,
}) as WeatherAppAction;


export type SetAvailableCitiesAction = {
    type: QueryActionType.SET_AVAILABLE_CITIES,
    citiesTrie: any,
}
export const MakeSetAvailableCitiesAction = (citiesTrie: any) => ({
    type: QueryActionType.SET_AVAILABLE_CITIES,
    citiesTrie,
}) as WeatherAppAction;

export type QueryAction =
      SetCityAction
    | SetAvailableCitiesAction
    | SetGeoLocAction
    | CommitQueryAction;
