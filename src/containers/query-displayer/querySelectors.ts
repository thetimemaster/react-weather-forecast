import {WeatherAppState} from "../../state";
import {QueryState} from "./queryState";
import {createSelector} from 'reselect';

export const selectQueryState = (state: WeatherAppState): QueryState => state.get("queryState");

export const selectQueryCity = createSelector(
    [selectQueryState],
    qs => qs.get("selectedCity")
);

export const selectAvailableCities = createSelector(
    [selectQueryState],
    qs => qs.get("availableCitiesTrie")
);

export const canSearch = createSelector(
    [selectQueryState],
    qs => qs.get("location") !== undefined
)

export const selectCoords = createSelector(
    [selectQueryState],
    qs => {
        const location = qs.get("location");
        return location === undefined
            ? undefined
            : {
                lat: location.get("lat"),
                lon: location.get("lng")
            }
    }
)

export const selectDescription = createSelector(
    [selectQueryState],
    qs => {
        const location = qs.get("location");
        return location === undefined
            ? undefined
            : location.get("description")
    }
)
