import * as Immutable from 'immutable';
const TrieSearch = require('trie-search');

export interface ICity {
    name: string
    coord: {
        lat: number,
        lon: number,
    }
}

export interface ICityTrie {
    get: (prefix: string) => ICity[]
}

export enum PredictionType {
    SHORT_TERM = 'Short',
    LONG_TERM = 'Long',
}

export type QueryState = Immutable.Record<{
    predictionType: PredictionType,
    availableCitiesTrie: ICityTrie,
    selectedCity: string,
    location: Immutable.Record<{
        description: string
        lat: number,
        lng: number,
    }> | undefined
}>;

export const defaultQueryState: QueryState = Immutable.fromJS({
    selectedCity: '',
    predictionType: PredictionType.SHORT_TERM,
    availableCitiesTrie: new TrieSearch('name', {min: 3}),
    location: undefined
});
