import {defaultQueryState, ICity, QueryState} from "./queryState";
import {QueryAction, QueryActionType} from "./queryActions";
import {Reducer} from 'redux';
import * as Immutable from 'immutable';

const queryReducer: Reducer<QueryState, QueryAction> =
(state: QueryState | undefined, action: QueryAction): QueryState => {

    if (state === undefined)
        state = defaultQueryState;

    switch (action.type) {
        case QueryActionType.SET_GEO_LOC:
            return state
                .set("selectedCity", '')
                .set("location", Immutable.fromJS({
                    description: 'Here',
                    lat: action.lat,
                    lng: action.lng,
                }))
        case QueryActionType.SET_CITY:
            let cities: ICity[] = state
                .get("availableCitiesTrie")
                .get(action.city);
            if (cities === undefined) cities = [];
            cities = cities
                .filter(city => city.name === action.city)

            const city = cities.length === 0
                ? undefined
                : cities[0];

            return state
                .set("selectedCity", action.city)
                .set("location",
                    city === undefined
                        ? undefined
                        : Immutable.fromJS({
                            description: city.name,
                            lat: city.coord.lat,
                            lng: city.coord.lon,
                        })
                );
        case QueryActionType.SET_AVAILABLE_CITIES:
            return state
                .set("availableCitiesTrie", action.citiesTrie);
        default:
            return state;
    }
};

export default queryReducer;
