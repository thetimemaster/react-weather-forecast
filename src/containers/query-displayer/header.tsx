import React, {Dispatch, FunctionComponent} from "react";
import AutocompleteSearch from "../../components/autocomplete-search/autocomplete-search";
import {connect} from "react-redux";
import {WeatherAppState} from "../../state";
import {MakeCommitQueryAction, MakeSetGeoLocAction, QueryActionType} from "./queryActions";
import {BaseActionType, WeatherAppAction} from "../../actions";
import styled from 'styled-components'
import {IStyleArgument} from "../../styles";
import Button, {ButtonFloat} from "../../components/button/Button";
import {canSearch, selectAvailableCities, selectQueryCity} from "./querySelectors";
import {ICity} from "./queryState";
import {MakeRequestLoadingAction} from "../weather-forecast-displayer/forecastActions";

type HeaderStateProps = {
    cities: ICity[]
    queryCity: string
    isValidCity: boolean
}

type HeaderDispatchProps = {
    onSelectedCity: (city: string) => void,
    toggleTheme: () => void,
    searchByGeoloc: () => void,
    searchByCity: () => void,
}

type HeaderProps = HeaderStateProps & HeaderDispatchProps;

const StyledHeader = styled.header`
  background-color: ${(props: IStyleArgument) => props.theme.primary};
  padding: ${(props: IStyleArgument) => props.theme.standardPadding};
  position: fixed;
  top: 0;
  left: 0;
  height: 50px;
  width: 100%;
`

const Header: FunctionComponent<HeaderProps> = props => (
    <StyledHeader>
        <AutocompleteSearch
            onSelect={props.onSelectedCity}
            items={props.cities.map(city => city.name)}
            currentValue={props.queryCity}
        />
        <Button text={'Search'} onClick={props.searchByCity} isDisabled={!props.isValidCity}/>
        <Button text={'Search by geoloc'} onClick={props.searchByGeoloc} isDisabled={false}/>
        <Button text={'Toggle theme'} onClick={props.toggleTheme} isDisabled={false} float={ButtonFloat.RIGHT}/>
    </StyledHeader>
);

const mapDispatchToProps = (dispatch: Dispatch<WeatherAppAction>): HeaderDispatchProps => {
    return {
        onSelectedCity: (city: string) => dispatch({
            type: QueryActionType.SET_CITY,
            city: city
        }),
        toggleTheme: () => dispatch({
            type: BaseActionType.TOGGLE_THEME,
        }),
        searchByGeoloc: () => {
            navigator.geolocation.getCurrentPosition(position => {
                dispatch(MakeSetGeoLocAction(
                    position.coords.latitude,
                    position.coords.longitude,
                ));
                dispatch(MakeRequestLoadingAction())
                dispatch(MakeCommitQueryAction())
            });
        },
        searchByCity: () => {
            dispatch(MakeRequestLoadingAction())
            dispatch(MakeCommitQueryAction())
        }

    }
}

const mapStateToProps = (state: WeatherAppState): HeaderStateProps => {
    const city = selectQueryCity(state)
    const cities = selectAvailableCities(state).get(city)
    return {
        cities: cities === undefined ? [] : cities,
        queryCity: city,
        isValidCity: canSearch(state),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
