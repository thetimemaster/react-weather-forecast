import {WeatherAppAction} from "../../actions";
import {DailyWeather, HourlyWeather} from "./forecastState";
const FORECAST_ACTION_PREFIX = 'forecast';

export const isForecastAction = (action: WeatherAppAction): action is ForecastAction => {
    return action.type.startsWith(FORECAST_ACTION_PREFIX);
}


export enum ForecastActionType {
    RECEIVE_API_DATA = 'forecast/ReceiveApiData',
    TOGGLE_FORECAST_MODE = 'forecast/ToggleMode',
    SET_GIF_URL = 'forecast/SetGifUrl',
    SELECT_SOME_GIF = 'forecast/RequestGifUrl',
    REQUEST_LOADING = 'forecast/RequestLoading',
}


export type ToggleLengthAction = {
    type: ForecastActionType.TOGGLE_FORECAST_MODE,
}
export const MakeToggleLengthAction = () => ({
    type: ForecastActionType.TOGGLE_FORECAST_MODE,
}) as WeatherAppAction;


export type RequestLoadingAction = {
    type: ForecastActionType.REQUEST_LOADING,
}
export const MakeRequestLoadingAction = () => ({
    type: ForecastActionType.REQUEST_LOADING,
}) as WeatherAppAction;


export type ReceiveApiDataAction = {
    type: ForecastActionType.RECEIVE_API_DATA
    data: {
        hourly: HourlyWeather[],
        daily: DailyWeather[],
    },
    newLocation: string,
}
// No explicit constructor - api response action


export type SetGifUrlAction = {
    type: ForecastActionType.SET_GIF_URL,
    url: string,
}
export const MakeSetGifUrlAction = (url: string) => ({
    type: ForecastActionType.SET_GIF_URL,
    url,
}) as WeatherAppAction;


export type SelectGifUrlAction = {
    type: ForecastActionType.SELECT_SOME_GIF,
    data: {
        results: {
           media: {
               gif: {
                   url: string
               }
           }[]
        }[]
    },
}
// No explicit constructor - api response action

export type ForecastAction =
    ReceiveApiDataAction
    | ToggleLengthAction
    | SetGifUrlAction
    | RequestLoadingAction
    | SelectGifUrlAction;
