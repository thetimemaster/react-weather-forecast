import React, {FunctionComponent} from "react";
import styled from "styled-components";
import CenterFill from "../../../../components/center-fill-wrapper/CenterFill";
import {IStyleArgument} from "../../../../styles";

const MainText = styled.h1`
  font-size: 4em;
  text-transform: uppercase;
  text-align: center;
  width: 70vw;
`

const SecondaryText = styled.p`
  font-size: 2em;
  text-align: center;
  width: 70vw;
`

const Wrapper = styled.div`
  border: 2px solid ${(p: IStyleArgument) => p.theme.primary}
`

const WelcomeScreen: FunctionComponent = () => (
    <CenterFill>
        <Wrapper>
            <MainText>
                {"Welcome to weather app."}
            </MainText>
            <SecondaryText>
                {"Select a city or use current location."}
            </SecondaryText>
        </Wrapper>
    </CenterFill>
)

export default WelcomeScreen;

