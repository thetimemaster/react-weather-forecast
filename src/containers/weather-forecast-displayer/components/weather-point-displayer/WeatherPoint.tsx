import React, {FunctionComponent} from "react";
import {NormalizedWeather} from "../../forecastState";
import {Record} from 'immutable';
import styled from "styled-components";
import {IStyleArgument} from "../../../../styles";
import WeatherIcon from "./weather-icon/WeatherIcon";

export type WeatherPointProps = {
    point: Record<NormalizedWeather>
}

const WeatherPointWrapper = styled.div`
  border: 2px solid ${(p: IStyleArgument) => p.theme.primary};
  display: inline-block;
  width: 250px;
  height: 100%;
  margin-left: 5px;
  margin-right: 5px;
  padding: 5px;
  vertical-align: top; 
`

const TimeHeader = styled.h2`
  font-size: 1.4em;
`

const WeatherPoint: FunctionComponent<WeatherPointProps> = (props: WeatherPointProps) => (
    <WeatherPointWrapper>
        <TimeHeader>
            {new Date(props.point.get("timestamp") * 1000).toUTCString()}
        </TimeHeader>
        <hr/>
        <p>{`Temperature MAX: ${Math.round(props.point.get("temperatureMax"))}°`}</p>
        <p>{`Temperature MIN: ${Math.round(props.point.get("temperatureMin"))}°`}</p>
        <p>{`Pressure: ${props.point.get("pressure")}hPa`}</p>
        <p>{`Rain: ${props.point.get("rain")}`}</p>
        <WeatherIcon openApiIconName={props.point.get("icon")}/>
    </WeatherPointWrapper>
);

export default WeatherPoint;
