import React, {FunctionComponent} from "react";
import styled from "styled-components";
import {IStyleArgument} from "../../../../../styles";

export type WeatherIconProps = {
    openApiIconName: string
}

const iconMap: {
    [s: string]: string
} = {
    '01d': '001-sunny',
    '01n': '014-waxing moon',
    '02d': '002-cloudy',
    '02n': '016-cloudy',
    '03d': '002-cloudy',
    '03n': '016-cloudy',
    '04d': '010-wind',
    '04n': '010-wind',
    '09d': '005-heavy rain',
    '09n': '005-heavy rainy',
    '10d': '004-rainy',
    '10n': '017-rain',
    '11d': '007-thunderstorm',
    '11n': '007-thunderstorm',
    '13d': '009-snow',
    '13n': '009-snow',
    '50d': '011-wind',
    '50n': '011-wind',
};

const mapOpenApiIconCode = (code: string): string => `${iconMap[code]}.svg`;

interface IWithSrc {
    src: string;
}

const Icon = styled.div<IWithSrc>`
 width: 80px;
 height: 80px;
 margin: auto;
 background-color: ${(p: IStyleArgument) => p.theme.primary}; /* defines the background color of the image */
 mask: url("${(p: IWithSrc) => p.src}") no-repeat center / contain;
 -webkit-mask: url("${(p: IWithSrc) => p.src}") no-repeat center / contain; 
`

const WeatherIcon: FunctionComponent<WeatherIconProps> = props =>
    <Icon src={`icons/${mapOpenApiIconCode(props.openApiIconName)}`}/>

export default WeatherIcon;
