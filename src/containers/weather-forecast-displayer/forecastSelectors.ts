import {ForecastState} from "./forecastState";
import * as Ramda from 'ramda';
import {WeatherAppState} from "../../state";
import {createSelector} from 'reselect';

const average = Ramda.converge(Ramda.divide, [Ramda.sum, Ramda.length]);

export const selectForecastState = (state: WeatherAppState): ForecastState => state.get("forecastState");

export const selectWeatherPoints = createSelector(
    [selectForecastState],
    fs => fs.get("showHourly")
            ? fs.get("hourly")
            : fs.get("daily")
)

export const selectAvgTemperature = createSelector(
    [selectWeatherPoints],
    points => average(points.map(
        p => (p.get("temperatureMin") + p.get("temperatureMax")) / 2,
    ))
);

export const selectMaxTemperature = createSelector(
    [selectWeatherPoints],
    points => Math.max(...points.map(p =>p.get("temperatureMax")).toArray())
);

export const selectMinTemperature = createSelector(
    [selectWeatherPoints],
    points => Math.min(...points.map(p =>p.get("temperatureMin")).toArray())
);

export const selectMaxRain = createSelector(
    [selectWeatherPoints],
    points => Math.max(...points.map(p =>p.get("rain")).toArray())
);

export const selectWeatherTier = createSelector(
    [selectMaxTemperature, selectMinTemperature, selectMaxRain, selectAvgTemperature],
    (maxT, minT, maxR, avgT) =>
        ((maxT < 30 && minT > 15) ? 1 : 0) +
        ((avgT < 25 && avgT > 18) ? 1 : 0) +
        (maxR === 0 ? 1 : 0)
)

export const selectGifKeyword = createSelector(
    [selectWeatherPoints],
    fs =>
        fs
        .map(point => point.get("weatherDesc"))
        .toArray()
        .sort()
        .reduce((obj, keyword) => {
            const isNewKeyword = obj.prevKeyword !== keyword;
            const newScore = isNewKeyword ? 1 : obj.prevScore + 1;
            return {
                bestKeyword: newScore > obj.bestScore ? keyword : obj.bestKeyword,
                bestScore: Math.max(newScore, obj.bestScore),
                prevKeyword: keyword,
                prevScore: newScore
            }
        }, {
            bestKeyword: '',
            bestScore: 0,
            prevKeyword: '__no_keyword',
            prevScore: 0,
        })
        .bestKeyword
    );

