import {defaultForecastState, ForecastState, normalizeDailyWeather, normalizeHourlyWeather} from "./forecastState";
import {Reducer} from 'redux';
import {ForecastAction, ForecastActionType} from "./forecastActions";
import * as Immutable from 'immutable';

const forecastReducer: Reducer<ForecastState, ForecastAction> =
    (state, action) => {

        if (state === undefined)
            state = defaultForecastState;

        state = state as ForecastState;

        switch (action.type) {
            case ForecastActionType.REQUEST_LOADING:
                return state
                    .set("showLoading", true)
            case ForecastActionType.SET_GIF_URL:
                return state
                    .set("currentGifUrl", action.url);
            case ForecastActionType.TOGGLE_FORECAST_MODE:
                return state
                    .set("showHourly", !state.get("showHourly"));
            case ForecastActionType.RECEIVE_API_DATA:
                return state
                    .set("showLoading", false)
                    .set("locationDescription", action.newLocation)
                    .set("hourly", Immutable.fromJS(
                        action.data.hourly.map(normalizeHourlyWeather)
                    ))
                    .set("daily", Immutable.fromJS(
                        action.data.daily.map(normalizeDailyWeather)
                    ))
            default:
                return state;
        }
    };

export default forecastReducer;
