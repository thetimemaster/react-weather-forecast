import * as Immutable from 'immutable';
import {KELVIN_TO_CELCIUS} from "../../const";

export type NormalizedWeather = {
    timestamp: number,
    temperatureMin: number,
    temperatureMax: number,
    pressure: number,
    humidity: number,
    weatherDesc: string,
    clouds: number,
    rain: number,
    icon: string
}

export type DailyWeather = {
    dt: number,
    sunrise: number,
    sunset: number,
    temp: {
        day: number,
        min: number,
        max: number,
        night: number,
        eve: number,
        morn: number
    },
    feels_like: {
        day: number,
        night: number,
        eve: number,
        morn: number
    },
    pressure: number,
    humidity: number,
    dew_point: number,
    wind_speed: number,
    wind_deg: number,
    weather: {
        id: number,
        main: string,
        description: string,
        icon: string
    }[],
    clouds: number,
    rain?: number,
    uvi: number
}

export type HourlyWeather = {
    dt: number,
    temp: number,
    feels_like: number,
    pressure: number,
    humidity: number,
    dew_point: number,
    clouds: number,
    wind_speed: number,
    wind_deg: number,
    weather: {
        id: number,
        main: string,
        description: string,
        icon: string
    }[],
    rain?: {
        "1h": number
    }
}

export type ForecastState = Immutable.Record<{
    showLoading: boolean,
    locationDescription: string,
    showHourly: boolean,
    currentGifUrl: string,
    daily: Immutable.List<Immutable.Record<NormalizedWeather>>
    hourly: Immutable.List<Immutable.Record<NormalizedWeather>>
}>

export const defaultForecastState: ForecastState = Immutable.fromJS({
    showLoading: false,
    locationDescription: '',
    showHourly: false,
    currentGifUrl: '',
    daily: [],
    hourly: [],
});

export const normalizeDailyWeather = (w: DailyWeather): NormalizedWeather => ({
    timestamp: w.dt,
    temperatureMin: w.temp.min + KELVIN_TO_CELCIUS,
    temperatureMax: w.temp.max + KELVIN_TO_CELCIUS,
    humidity: w.humidity,
    pressure: w.pressure,
    weatherDesc: w.weather[0].description,
    clouds: w.clouds,
    rain: w.rain ? w.rain : 0,
    icon: w.weather[0].icon,
});

export const normalizeHourlyWeather =  (w: HourlyWeather): NormalizedWeather => ({
    timestamp: w.dt,
    temperatureMin: w.temp + KELVIN_TO_CELCIUS,
    temperatureMax: w.temp + KELVIN_TO_CELCIUS,
    humidity: w.humidity,
    pressure: w.pressure,
    weatherDesc: w.weather[0].description,
    clouds: w.clouds,
    rain: w.rain ? w.rain["1h"] : 0,
    icon: w.weather[0].icon,
});
