import React, {Dispatch, FunctionComponent} from "react";
import {selectForecastState, selectWeatherPoints, selectWeatherTier} from "./forecastSelectors";
import {WeatherAppState} from "../../state";
import {connect} from "react-redux";
import styled from "styled-components";
import {IStyleArgument} from "../../styles";
import {List, Record} from 'immutable';
import {NormalizedWeather} from "./forecastState";
import WeatherPoint from "./components/weather-point-displayer/WeatherPoint";
import Button from "../../components/button/Button";
import {WeatherAppAction} from "../../actions";
import {MakeToggleLengthAction} from "./forecastActions";
import FillLoader from "../../components/fill-loader/FillLoader";
import ImageWithLoader from "../../components/image-with-loader/ImageWithLoader";
import WelcomeScreen from "./components/welcome-screen/WelcomeScreen";

export type WeatherForecastProps = {
    isHourly: boolean,
    toggleHourly: () => void,
    locationDesc: string,
    gifUrl: string,
    showLoading: boolean,
    weatherPoints: List<Record<NormalizedWeather>>
    weatherTier: string
}

const ForecastWrapper = styled.div`
  background-color: ${(p: IStyleArgument) => p.theme.background};
  display: inline-block;
  height: 100vh;
  width: 100vw;
  box-sizing: border-box;
  padding: ${(p: IStyleArgument) => p.theme.standardPadding};
  color: ${(p: IStyleArgument) => p.theme.text};
  padding-top: 50px;
`

const GifWrapper = styled.div`
  height: 300px;
  min-height: 300px;
`

const PointWrapper = styled.div`
  overflow-x: auto;
  height: 420px;
  white-space: nowrap;
  line-height: 0;
  overflow-y: hidden;
  padding: 5px 5px 15px;
  
  & div {
    white-space: normal;
    line-height: initial;
  }
`

const WeatherForecast: FunctionComponent<WeatherForecastProps> = (props: WeatherForecastProps) => {
    if (props.showLoading) {
        return (
            <ForecastWrapper>
                <FillLoader/>
            </ForecastWrapper>
        );
    }

    if (props.locationDesc === '') {
        return (
            <ForecastWrapper>
                <WelcomeScreen/>
            </ForecastWrapper>
        )
    }
    return (
        <ForecastWrapper>
            <Button
                text={(
                    props.isHourly
                        ? 'Set to daily'
                        : 'Set to hourly'
                )}
                onClick={props.toggleHourly}
                isDisabled={false}
            />
            <h2>
                {`Weather in ${props.locationDesc} is ${props.weatherTier}`}
            </h2>
            <GifWrapper>
                <ImageWithLoader src={props.gifUrl}/>
            </GifWrapper>
            <PointWrapper>
                {props.weatherPoints.map(
                    (point, i) => <WeatherPoint key={i} point={point}/>
                )}
            </PointWrapper>
        </ForecastWrapper>
    )
}

const mapStateToProps = (state: WeatherAppState) => {
    const weatherTierNo = selectWeatherTier(state);
    const weatherTierStr = weatherTierNo === 3
        ? 'nice'
        : weatherTierNo === 2
            ? 'passable'
            : 'not nice'

    return {
        showLoading: selectForecastState(state).get("showLoading"),
        locationDesc: selectForecastState(state).get("locationDescription"),
        gifUrl: selectForecastState(state).get("currentGifUrl"),
        weatherPoints: selectWeatherPoints(state),
        isHourly: selectForecastState(state).get("showHourly"),
        weatherTier: weatherTierStr,
    };
}

const mapDispatchToProps = (dispatch: Dispatch<WeatherAppAction>) => {
    return {
        toggleHourly: () => dispatch(MakeToggleLengthAction())
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WeatherForecast);
