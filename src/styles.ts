export interface ITheme {
    primary: string,
    primaryDark: string,
    secondary: string,
    secondaryDark: string
    background: string,
    text: string,
    textOnPrimary: string,
    textOnSecondary: string,
    standardPadding: string
}

const lightTheme: ITheme = {
    primary: 'green',
    primaryDark: 'darkgreen',
    secondary: 'cornflowerblue',
    secondaryDark: 'blue',
    background: 'white',
    text: 'black',
    textOnPrimary: 'black',
    textOnSecondary: 'black',
    standardPadding: '5px',
};

const darkTheme: ITheme = {
    primary: 'blueviolet',
    primaryDark: 'darkviolet',
    secondary: 'mediumpurple',
    secondaryDark: 'rebeccapurple',
    background: '#222',
    text: 'white',
    textOnPrimary: 'black',
    textOnSecondary: 'black',
    standardPadding: '5px',
};

export const Themes = {
    LIGHT: lightTheme,
    DARK: darkTheme,
};

export interface IStyleArgument {
    theme: ITheme
}
