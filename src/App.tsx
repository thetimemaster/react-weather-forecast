import * as React from 'react';
import {Dispatch, useEffect} from 'react';
import Header from "./containers/query-displayer/header";
import WeatherForecast from "./containers/weather-forecast-displayer/WeatherForecast";
import * as Styled from 'styled-components'
import {IStyleArgument, ITheme, Themes} from "./styles";
import {connect, useDispatch} from "react-redux";
import {WeatherAppState} from "./state";
import {DEBUG, Theme} from "./const";
import {MakeSetAvailableCitiesAction} from "./containers/query-displayer/queryActions";
import {WeatherAppAction} from "./actions";
import IconAuthorAttribute from "./components/icon-author-attribute/IconAuthorAttribute";
const citiesAll = require('./cities/city.list.json');
const citiesSome = require('./cities/city.list.short.json');
const TrieSearch = require('trie-search');

type AppProps = {
    theme: ITheme
}

const GlobalStyle = Styled.createGlobalStyle`
    body {
        font-family: Sans, serif;
        min-height: 100vh;
        overflow: hidden;
        
        background-color: ${(p: IStyleArgument) => p.theme.background};
    }
`

const generateCitiesTrie = (dispatch: Dispatch<WeatherAppAction>) => () => {
    const cities = DEBUG ? citiesSome : citiesAll;
    const trie = new TrieSearch('name', {min: 3});
    trie.addAll(cities);
    dispatch(MakeSetAvailableCitiesAction(trie));
}

const App: React.FunctionComponent<AppProps> = (props) => {

    const dispatch = useDispatch<Dispatch<WeatherAppAction>>();

    useEffect(generateCitiesTrie(dispatch), [dispatch]);

    return (
        <Styled.ThemeProvider theme={props.theme}>
            <GlobalStyle/>
            <Header />
            <WeatherForecast/>
            <IconAuthorAttribute/>
        </Styled.ThemeProvider>
    );
}

const mapStateToProps = (state: WeatherAppState) => ({
    theme: state.get("theme") === Theme.LIGHT
        ? Themes.LIGHT
        : Themes.DARK
});

export default connect(mapStateToProps)(App);
