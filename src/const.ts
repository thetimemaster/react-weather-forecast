export enum Theme {
    LIGHT = 'LIGHT',
    DARK = 'DARK',
}

export const KELVIN_TO_CELCIUS = -273.15;

export const DEBUG = false;
