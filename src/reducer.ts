import {defaultState, WeatherAppState} from "./state";
import {BaseActionType, WeatherAppAction} from "./actions";
import {Reducer} from "redux";
import {isQueryAction, QueryAction,} from "./containers/query-displayer/queryActions";
import queryReducer from "./containers/query-displayer/queryReducer";
import {Theme} from "./const";
import {ForecastAction, isForecastAction} from "./containers/weather-forecast-displayer/forecastActions";
import forecastReducer from "./containers/weather-forecast-displayer/forecastReducer";

export const appReducer: Reducer<WeatherAppState, WeatherAppAction> =
    (state: WeatherAppState | undefined, action: WeatherAppAction): WeatherAppState => {

    if (state === undefined)
        state = defaultState;

    if (isQueryAction(action)) {
        return state
            .set("queryState",
                queryReducer(state.get("queryState"), action as QueryAction)
            );
    }

    if (isForecastAction(action)) {
        return state
            .set("forecastState",
                forecastReducer(state.get("forecastState"), action as ForecastAction)
            );
    }

    switch (action.type) {
        case (BaseActionType.CACHE_API_RESPONSE):
            return state
                .setIn(["ajaxCache", action.route], action.result);
        case (BaseActionType.TOGGLE_THEME):
            return state
                .set("theme", state.get("theme") === Theme.LIGHT
                    ? Theme.DARK
                    : Theme.LIGHT
                );
        default:
            return state;
    }
}
