import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as Redux from 'redux';
import {applyMiddleware} from 'redux';
import {WeatherAppAction} from "./actions";
import {appReducer} from "./reducer";
import {WeatherAppState} from "./state";
import {Provider} from "react-redux";
import {DEBUG} from "./const";
import {combineEpics, createEpicMiddleware} from "redux-observable";
import {apiFetcherEpic} from "./epics/apiFetcherEpic";
import {weatherMapEpic} from "./epics/weatherMapEpic";
import {gifFetcherEpic} from "./epics/gitSwitcherEpic";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof Redux.compose;
    }
}

const rootEpic = combineEpics(
    apiFetcherEpic,
    weatherMapEpic,
    gifFetcherEpic,
);
const epicMiddleware = createEpicMiddleware<WeatherAppAction, WeatherAppAction, WeatherAppState>();

const composeEnhancers = DEBUG
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || Redux.compose
    : Redux.compose;

const store =  Redux.createStore<WeatherAppState, WeatherAppAction, any, any>(appReducer, composeEnhancers(
    applyMiddleware(epicMiddleware)
));


epicMiddleware.run(rootEpic);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);
